import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';  //+ اضف

@Injectable()
export class CarProvider {

  constructor(public http: HttpClient) {
  }

  getCars(){  //+ اضف
    let url:string='assets/data/cars.json';
    return  this.http.get(url)
      .pipe(
        map(res => res) 
      );
  }

}
