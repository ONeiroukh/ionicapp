import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { map } from 'rxjs/operators';  //+
import { CarProvider } from './../../providers/car/car';  //+

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  cars:any=[];

  constructor(
    public navCtrl: NavController,
    public carProvider: CarProvider,
  ) {

  
    this.carProvider.getCars()
    .subscribe(
        result => {
           let response:any = result; 
           this.cars = response; 
           console.log(result); // + من خلال debugger من اجل فحص البيانات بالمتصفح

        },
        error => {
            console.log(error);
        }
    );
   

    /*this.cars=[
      {
        id:1,
        image:'assets/images/image1.jpg',
        reference:'IBIZA',
        brand:'Seat',
       },
       {
        id:2,
        image:'assets/images/image2.jpg',
        reference:'Sandero',
        brand:'Dacia',
       },
       {
        id:3,
        image:'assets/images/image3.jpg',
        reference:'Symbol',
        brand:'Renault',
       },

       {
        id:4,
        image:'assets/images/image4.jpg',
        reference:'307',
        brand:'Peugeot',
       },
    ];
   */
}   

   

}
