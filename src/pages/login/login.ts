import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'; 
import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public _loginForm:FormGroup;
  public _submitted:boolean = false;
  public errorMessage:string=null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authenticationProvider: AuthenticationProvider,
    public _formBuilder:FormBuilder,

    ) {
      this.createForm(); // تمهيد حقول فورم تسجيل الدخول مباشرة بعد اقلاع الصفحة
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


  public createForm(){
    this._loginForm = this._formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
    });
}


  homePage(){ // في اعلى صفحة تسجيل الدخول ستجد زر يوجهك نحو الصفحة الرئيسية
    this.navCtrl.push(HomePage); 
  }

  registerPage(){ // الزر الدي يوجهك نحو صفحة تسجيل الدخول
    this.navCtrl.push(RegisterPage); 
  }

  onSubmit(dataForm: any) {
    this._submitted = true;  //handler مقبض يعلن عن انه تم الضغط على زر الدخول
    console.log(dataForm);
  }

}