import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';


@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  public user:any ={
    'username':'exportdeveloper',
    'firstname':'Seif eddine',
    'lastname':'Seif eddine',
    'email':'contact@exportdeveloper.com',
  };

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  
  homePage(){ // في اعلى صفحة تسجيل الدخول ستجد زر يوجهك نحو الصفحة الرئيسية
    this.navCtrl.push(HomePage); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

}