import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'; 
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';
 
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  public _registerForm:FormGroup;
  public _submitted:boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authenticationProvider: AuthenticationProvider,
    public _formBuilder:FormBuilder,
    ) {
      this.createForm(); // تمهيد حقول فورم تسجيل الدخول مباشرة بعد اقلاع الصفحة
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

public createForm(){
    this._registerForm = this._formBuilder.group({
      username: ['', Validators.required],
      lastname: ['', Validators.required],
      firstname: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
    });
}

  homePage(){ // في اعلى صفحة تسجيل الدخول ستجد زر يوجهك نحو الصفحة الرئيسية
    this.navCtrl.push(HomePage); 
  }

  loginPage(){ // الزر الدي يوجهك نحو صفحة تسجيل الدخول
    this.navCtrl.push(LoginPage); 
  }
  
  onSubmit(dataForm: any) {
    this._submitted = true;
    console.log('submitted');
  }
}